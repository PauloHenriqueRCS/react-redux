import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';

const API_KEY = 'AIzaSyD5bNxLr0KQCwXDp5s5PQC448andwaKf2w';

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            onSelectVideo: null
        };

        this.videoSearch('top battes hunter x hunter');
    }

    videoSearch(term) {
        YTSearch({ key: API_KEY, term: term }, (videos) => {
            this.setState({
                videos: videos,
                selectdVideo: videos[0]
            });
        });
    }

    render() {
        const videoSearch = _.debounce((term) => { this.videoSearch(term) }, 300);

        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectdVideo} />
                <VideoList
                    onVideoSelect={selectdVideo => this.setState({ selectdVideo })}
                    videos={this.state.videos} />
            </div>
        );
    }

}

ReactDOM.render(<App />, document.querySelector('.container'));